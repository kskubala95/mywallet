package com.example.krzysztof.mywallet;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Krzysztof on 2017-04-16.
 */

public class GetRegister extends StringRequest {


    private static final String GET_REGISTER_URL = "https://skubalawrobel.000webhostapp.com/Register.php";
    private Map <String, String> params;

    public GetRegister ( String name, String username, String password, String email, Response.Listener <String> listener ) {
        super ( Method.POST, GET_REGISTER_URL, listener, null );
        params = new HashMap <> ( );
        params.put ( "name", name );
        params.put ( "username", username );
        params.put ( "password", password );
        params.put ( "email", email + "" );
    }

    @Override
    public Map <String, String> getParams ( ) {
        return params;
    }
}