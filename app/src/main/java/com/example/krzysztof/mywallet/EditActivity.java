package com.example.krzysztof.mywallet;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


/**
 * Created by Krzysztof on 2017-04-19.
 */

public class EditActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit );

        final EditText account_value = (EditText) findViewById(R.id.account_value);
        final Button confirm = ( Button ) findViewById ( R.id.confirm );

        confirm.setOnClickListener ( new View.OnClickListener ( ) {
            @Override
            public void onClick ( View v ) {
                final String _account_value = account_value.getText ().toString ();
                Intent intent= new Intent ( EditActivity.this,AccountActivity.class );
                intent.putExtra ( "account_value",_account_value);
                EditActivity.this.startActivity ( intent );

            }
        } );
    }
}