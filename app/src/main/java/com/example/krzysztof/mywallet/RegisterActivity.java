package com.example.krzysztof.mywallet;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_register );

        final EditText Name = ( EditText ) findViewById ( R.id.Name );
        final EditText Username = ( EditText ) findViewById ( R.id.Username );
        final EditText Password = ( EditText ) findViewById ( R.id.Password );
        final EditText Email = ( EditText ) findViewById ( R.id.Email );
        final Button confirm = ( Button ) findViewById ( R.id.confirm );

        confirm.setOnClickListener ( new View.OnClickListener ( ) {
            @Override
            public void onClick ( View v ) {
                final String name = Name.getText ( ).toString ( );
                final String username = Username.getText ( ).toString ( );
                final String password = Password.getText ( ).toString ( );
                final String email = Email.getText ( ).toString ( );

                Response.Listener <String> responseListener = new Response.Listener <String> ( ) {
                    @Override
                    public void onResponse ( String response ) {
                        try {
                            JSONObject jsonResponse = new JSONObject ( response );
                            boolean success = jsonResponse.getBoolean ( "success" );
                            if ( success ) {
                                Intent intent = new Intent ( RegisterActivity.this, LoginActivity.class );
                                RegisterActivity.this.startActivity ( intent );
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder ( RegisterActivity.this );
                                builder.setMessage ( "Register failed" ).setNegativeButton ( "retry", null ).create ( ).show ( );
                            }
                        } catch ( JSONException e ) {
                            e.printStackTrace ( );
                        }
                    }
                };
                GetRegister getRegister = new GetRegister ( name, username, password, email, responseListener );
                RequestQueue queue = Volley.newRequestQueue(RegisterActivity.this);
                queue.add(getRegister);
            }
        } );
    }
}