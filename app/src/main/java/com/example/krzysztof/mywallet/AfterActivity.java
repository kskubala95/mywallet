package com.example.krzysztof.mywallet;

/** Edit jako nowa aktywnosc, gdzie mozna wprowadzać zmiany w stanie, zasilac, obciazac ?????  jest to response do aftera
 *  AfterActivity: dodać progress bar; członkowie rodziny "family" (button pod account); jakis bilans na co ile poszlo itp
 *  Trzeba validate do logowania zrobić
 *  getIntent(); !!
 * **/

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.krzysztof.mywallet.R;

public class AfterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_after );

        final Button account = ( Button ) findViewById ( R.id.account );
        final Button logout = ( Button ) findViewById ( R.id.logout );
        final Button family = ( Button ) findViewById ( R.id.family );
        final ProgressBar prog_bar = (ProgressBar ) findViewById ( R.id.prog_bar );

    prog_bar.setProgress (  );


        account.setOnClickListener ( new View.OnClickListener ( ) {
            @Override
            public void onClick ( View v ) {
                Intent post_account = new Intent (AfterActivity.this, com.example.krzysztof.mywallet.AccountActivity.class);
                AfterActivity.this.startActivity(post_account);
            }
        } );

        logout.setOnClickListener ( new View.OnClickListener ( ) {
            @Override
            public void onClick ( View v ) {
                Intent _log_out = new Intent (AfterActivity.this, com.example.krzysztof.mywallet.LoginActivity.class);
                AfterActivity.this.startActivity(_log_out);
            }
        } );

        family.setOnClickListener ( new View.OnClickListener ( ) {
            @Override
            public void onClick ( View v ) {
                Intent _log_out = new Intent (AfterActivity.this, com.example.krzysztof.mywallet.FamilyActivity.class);
                AfterActivity.this.startActivity(_log_out);
            }
        } );

    }
}