package com.example.krzysztof.mywallet;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by Krzysztof on 2017-04-18.
 */

/**
 * Edit jako nowa aktywnosc, gdzie mozna wprowadzać zmiany w stanie, zasilac, obciazac ?????  jest to response do aftera
 * AfterActivity: dodać progress bar; członkowie rodziny "family" (button pod account); jakis bilans na co ile poszlo itp
 * Trzeba validate do pa
 **/


public class AccountActivity extends AppCompatActivity {

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_account );

        final Button Edit_acc = ( Button ) findViewById ( R.id.Edit_acc );
        final TextView s_account_value = ( TextView ) findViewById ( R.id.s_account_value );
        final Button back_button = ( Button ) findViewById ( R.id.back_button );


        final Bundle extras = getIntent ( ).getExtras ( );

        if ( extras != null ) {
            final String temp = extras.getString ( "account_value" );
            s_account_value.setText ( temp );
        }
        Edit_acc.setOnClickListener ( new View.OnClickListener ( ) {
            @Override
            public void onClick ( View v ) {
                Intent post_account = new Intent ( AccountActivity.this, com.example.krzysztof.mywallet.EditActivity.class );
                AccountActivity.this.startActivity ( post_account );
            }
        } );

        back_button.setOnClickListener ( new View.OnClickListener ( ) {
            @Override
            public void onClick ( View v ) {
                Intent back_account = new Intent ( AccountActivity.this, com.example.krzysztof.mywallet.AfterActivity.class );
                AccountActivity.this.startActivity ( back_account );
            }
        } );

    }
}