package com.example.krzysztof.mywallet;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;
import com.example.krzysztof.mywallet.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.jar.Attributes;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_login );

        final EditText Username = ( EditText ) findViewById ( R.id.Username );
        final EditText Password = ( EditText ) findViewById ( R.id.Password );
        final Button SignIn = ( Button ) findViewById ( R.id.SignIn );
        final TextView register = ( TextView ) findViewById ( R.id.register );

        register.setOnClickListener ( new View.OnClickListener ( ) {
            @Override
            public void onClick ( View v ) {
                Intent post_register = new Intent ( LoginActivity.this, com.example.krzysztof.mywallet.RegisterActivity.class );
                LoginActivity.this.startActivity ( post_register );
            }
        } );

        SignIn.setOnClickListener ( new View.OnClickListener ( ) {
            @Override
            public void onClick ( View v ) {
                final String username = Username.getText ( ).toString ( );
                final String password = Password.getText ( ).toString ( );
                Response.Listener <String> responseListener = new Response.Listener <String> ( ) {
                    @Override
                    public void onResponse ( String response ) {
                        try {
                            JSONObject jsonResponse = new JSONObject ( response );
                            boolean success = jsonResponse.getBoolean ( "success" );
                            if ( success ) {
                                String name = jsonResponse.getString ( "name" );
                                String email = jsonResponse.getString ( "email" );
                                Intent intent = new Intent ( LoginActivity.this, AfterActivity.class );
                                intent.putExtra ( "name", name );
                                intent.putExtra ( "username", username );
                                intent.putExtra ( "email", email );
                                LoginActivity.this.startActivity ( intent );
                                Context context = getApplicationContext();
                                CharSequence text = "Pomyslnie zalogowano!";
                                int duration = Toast.LENGTH_SHORT;

                                Toast toast = Toast.makeText(context, text, duration);
                                toast.show();
                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder ( LoginActivity.this );
                                builder.setMessage ( "Login failed" ).setNegativeButton ( "Retry", null ).create ( ).show ( );
                            }
                        } catch ( JSONException e ) {
                            e.printStackTrace ( );
                        }
                    }
                };
                GetLogin getLogin = new GetLogin ( username, password, responseListener );
                RequestQueue queue = Volley.newRequestQueue ( LoginActivity.this );
                queue.add ( getLogin );
            }
        } );
    }
}