package com.example.krzysztof.mywallet;

/**
 * Created by Krzysztof on 2017-04-16.
 */
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class GetLogin extends StringRequest {
    private static final String GET_LOGIN_URL = "https://skubalawrobel.000webhostapp.com/Login.php";
    private Map<String, String> params;

    public GetLogin(String username, String password, Response.Listener<String> listener) {
        super(Method.POST, GET_LOGIN_URL, listener, null);
        params = new HashMap<>();
        params.put("username", username);
        params.put("password", password);
    }

    @Override
    public Map<String, String> getParams() {
        return params;
    }
}