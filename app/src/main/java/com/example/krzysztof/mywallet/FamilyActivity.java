package com.example.krzysztof.mywallet;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;


/**
 * Created by Krzysztof on 2017-04-19.
 */

public class FamilyActivity extends ListActivity {
    ArrayList<String> listItems=new ArrayList<String>();
    ArrayAdapter<String> adapter;
    String str;
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_family);


        adapter=new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                listItems);
        setListAdapter(adapter);
    }
    public void addItems(View v) {
        EditText t_add_member= (EditText) findViewById ( R.id.t_add_member );
        str = t_add_member.getText ().toString ();
        listItems.add(str);
        adapter.notifyDataSetChanged();
    }
}